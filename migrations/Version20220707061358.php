<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220707061358 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE setting (id INT AUTO_INCREMENT NOT NULL, site_title VARCHAR(255) NOT NULL, logo_name VARCHAR(255) NOT NULL, logo_size INT NOT NULL, updated_at DATETIME NOT NULL, admin_mail VARCHAR(255) NOT NULL, lesson_time INT NOT NULL, color_type VARCHAR(255) NOT NULL, stripe_sk VARCHAR(255) NOT NULL, agora_app_id VARCHAR(255) NOT NULL, agora_app_certificate VARCHAR(255) NOT NULL, timezone VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE setting');
    }
}
