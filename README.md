# Online course platform
I developed an application that allows anyone who wants to teach something online to easily create an online course platform.

Due to the impact of the pandemic, more and more people are learning online.
Many people want to use their free time to share their particular skills with others online.

There is a wide range of online services available, but it takes time to stand out among the many sites with large numbers of registered users and to attract customers.

If people who want to teach something online can have their own website, they can do the marketing and SEO themselves, which will attract more customers. However, this would be very expensive.

So I created an application that allows these people to easily create their own online class.

This app has all the features required for online classes.
Many features, such as booking a lesson, taking an online lesson via video call, paying for the lesson ticket and contact between students and teacher can be done on the same platform.

To realize my project, I use php with the symfony framework which allows me to implement the different features.

I also used Agora Video API to implement the video call functionality, Stripe API for the payment.

## Requirement
- PHP >= 7.4
- MySQL 8.0
- Composer
- Yarn
- [Stripe account](https://stripe.com/)
- [Agora account](https://www.agora.io/)

## Install
1. Download the repository
2. Edit `DATABASE_URL` and `MAILER_DSN` in the .env file
3. Run `composer install`
4. Run `yarn install` and `yarn build`
5. Create database
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
```

## Local test
[ngrok](https://ngrok.com/) is useful for testing webhooks in a local environment
```
ngrok http http://localhost:3000
```
