function getTimeZone() {
  let offset = new Date().getTimezoneOffset(),
    o = Math.abs(offset);
  return (
    (offset < 0 ? "+" : "-") +
    ("00" + Math.floor(o / 60)).slice(-2) +
    ":" +
    ("00" + (o % 60)).slice(-2)
  );
}
console.log(getTimeZone());

getDevices = async () => {
  const location = window.location.hostname;
  const settings = {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: {
      timezone: getTimeZone(),
    },
  };
  try {
    const fetchResponse = await fetch(
      `http://${location}:8000/api/timezone`,
      settings
    );
    const data = await fetchResponse.json();
    return data;
  } catch (e) {
    return e;
  }
};
const timeButton = document.getElementById("get-timezone");
if (timeButton) {
  timeButton.addEventListener("click", function (e) {
    getDevices();
  });
}
