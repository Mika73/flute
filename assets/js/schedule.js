const location = window.location.hostname;

let scheduleOnOffApiurl = `https://${location}/admin/api/schedule/update`;
let reservationApiurl = `https://${location}/api/reservation/update`;
let ticketCountApiurl = `https://${location}/api/ticket/count`;

if (location == "localhost") {
  scheduleOnOffApiurl = `http://${location}:3000/admin/api/schedule/update`;
  reservationApiurl = `http://${location}:3000/api/reservation/update`;
  ticketCountApiurl = `http://${location}:3000/api/ticket/count`;
}

const onOffs = document.getElementsByClassName("btn-on-off");
const reservations = document.getElementsByClassName("btn-reservation");
const confirmDates = document.getElementsByClassName("confirm-date");
const confirmModal = document.getElementById("reserv-confirm-modal");
const cancelModal = document.getElementById("reserv-cancel-modal");
const closeBtns = document.getElementsByClassName("btn-modal-close");
const confirmBtn = document.getElementById("confirm-reservation-btn");
const cancelBtn = document.getElementById("cancel-reservation-btn");
const currentTicket = document.getElementById("confirm-current-ticket");
const noTickets = document.getElementsByClassName("no-ticket");
const haveTickets = document.getElementsByClassName("have-ticket");

/**
 * Call the API to obtain the number of tickets held by the user and display them in the navigation bar
 * @returns
 */
fetchTicketNumber = async () => {
  try {
    const fetchResponse = await fetch(ticketCountApiurl);
    const data = await fetchResponse.json();

    const numbers = document.getElementsByClassName("ticket-number");
    if (!numbers) {
      return;
    }

    Array.from(numbers).forEach((number) => {
      number.textContent = data.ticketNumber.toString();
      number.style.display = "inline-block";
    });
    return;
  } catch (e) {
    return e;
  }
};
/**
 * Wait for schedule input from admins and update schedule table
 * @param DATETIME, Click Element
 * @return
 */

updateSchedule = async (date, clickElement) => {
  const data = {
    "date-time": date,
  };
  const settings = {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  };
  try {
    const fetchResponse = await fetch(scheduleOnOffApiurl, settings);
    const data = await fetchResponse.json();

    clickElement.innerHTML = data.status;

    if (data.status == "Available") {
      clickElement.classList.remove("btn-secondary");
      clickElement.classList.add("btn-warning");
    } else {
      clickElement.classList.remove("btn-warning");
      clickElement.classList.add("btn-secondary");
    }
    return;
  } catch (e) {
    return e;
  }
};
/**
 * Update user information in the schedule table and the number of tickets in the user table
 * @param DATETIME, User ID, Click Element
 * @returns TRUE or FALSE
 */
updateReservation = async (date, user, clickElement) => {
  const data = {
    "date-time": date,
    user: user,
  };
  const settings = {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  };
  try {
    const fetchResponse = await fetch(reservationApiurl, settings);
    const data = await fetchResponse.json();

    if (data.status == "noTicket") {
      return false;
    }

    if (data.status == "Reserved") {
      clickElement.classList.remove("btn-info");
      clickElement.classList.add("btn-warning");
      clickElement.setAttribute("data-status", "reserved");
    } else {
      clickElement.classList.remove("btn-warning");
      clickElement.classList.add("btn-info");
      clickElement.setAttribute("data-status", "vacant");
    }

    Array.from(reservations).forEach((reservation) => {
      reservation.setAttribute("data-ticket-count", data.ticketNo);
    });

    fetchTicketNumber();

    return true;
  } catch (e) {
    return false;
  }
};

fetchTicketNumber();

Array.from(onOffs).forEach((onOff) => {
  onOff.addEventListener("click", function (e) {
    const clickElement = e.target;
    let date = clickElement.getAttribute("data-date");
    const time = clickElement.getAttribute("data-time");
    date = date + " " + time;
    updateSchedule(date, clickElement);
  });
});

Array.from(reservations).forEach((reservation) => {
  reservation.addEventListener("click", function (e) {
    const clickElement = e.target;
    const date = clickElement.getAttribute("data-date");
    const userDate = clickElement.getAttribute("user-date");
    const user = clickElement.getAttribute("data-user");
    const ticketCount = clickElement.getAttribute("data-ticket-count");
    const resevationStatus = clickElement.getAttribute("data-status");

    Array.from(confirmDates).forEach((confirmDate) => {
      confirmDate.innerHTML = userDate.slice(0, -3);
    });
    currentTicket.innerHTML = ticketCount;

    if (resevationStatus == "vacant") {
      if (ticketCount == 0) {
        Array.from(noTickets).forEach((noTicket) => {
          noTicket.style.display = "block";
        });
        Array.from(haveTickets).forEach((haveTicket) => {
          haveTicket.style.display = "none";
        });
        confirmBtn.disabled = true;
      } else {
        Array.from(noTickets).forEach((noTicket) => {
          noTicket.style.display = "none";
        });
        Array.from(haveTickets).forEach((haveTicket) => {
          haveTicket.style.display = "block";
        });
        confirmBtn.disabled = false;
      }
      confirmModal.style.display = "block";
    } else {
      cancelModal.style.display = "block";
    }

    Array.from(closeBtns).forEach((closeBtn) => {
      closeBtn.onclick = function () {
        confirmModal.style.display = "none";
        cancelModal.style.display = "none";
      };
    });

    confirmBtn.onclick = function () {
      const result = updateReservation(date, user, clickElement);

      result.then(function (data) {
        if (data) {
          confirmModal.style.display = "none";
        }
      });
    };

    cancelBtn.onclick = function () {
      updateReservation(date, user, clickElement);
      cancelModal.style.display = "none";
    };
  });
});
