// Go to top button
const topBtn = document.getElementById("goTopBtn");

if (topBtn) {
  topBtn.addEventListener("click", (e) => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  });
  window.onscroll = () => {
    if (
      document.body.scrollTop > 20 ||
      document.documentElement.scrollTop > 20
    ) {
      topBtn.style.display = "block";
      window.addEventListener("beforeprint", (e) => {
        topBtn.style.display = "none";
      });
    } else {
      topBtn.style.display = "none";
    }
  };
}
