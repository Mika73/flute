<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Repository\SettingRepository;

/**
 * @ORM\Entity(repositoryClass=SettingRepository::class)
 * @Vich\Uploadable
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $siteTitle;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="logo_image", fileNameProperty="logoName", size="logoSize")
     *
     * @var File|null
     */
    private $logoFile;

    /**
     * @ORM\Column(type="string")
     *
     * @var string|null
     */
    private $logoName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int|null
     */
    private $logoSize;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adminMail;

    /**
     * @ORM\Column(type="integer")
     */
    private $lessonTime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $colorType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $stripeSk;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $agoraAppId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $agoraAppCertificate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $timezone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $currency;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freeTicket;

    /**
     * @ORM\Column(type="integer")
     */
    private $minBeforeEntry;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currencySymbol;

    /**
     * @ORM\Column(type="integer")
     */
    private $minAfterEntry;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiteTitle(): ?string
    {
        return $this->siteTitle;
    }

    public function setSiteTitle(string $siteTitle): self
    {
        $this->siteTitle = $siteTitle;

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setLogoFile(?File $logoFile = null): void
    {
        $this->logoFile = $logoFile;

        if (null !== $logoFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }

    public function setLogoName(?string $logoName): void
    {
        $this->logoName = $logoName;
    }

    public function getLogoName(): ?string
    {
        return $this->logoName;
    }

    public function setLogoSize(?int $logoSize): void
    {
        $this->logoSize = $logoSize;
    }

    public function getlogoSize(): ?int
    {
        return $this->logoSize;
    }

    public function getAdminMail(): ?string
    {
        return $this->adminMail;
    }

    public function setAdminMail(string $adminMail): self
    {
        $this->adminMail = $adminMail;

        return $this;
    }

    public function getLessonTime(): ?int
    {
        return $this->lessonTime;
    }

    public function setLessonTime(int $lessonTime): self
    {
        $this->lessonTime = $lessonTime;

        return $this;
    }

    public function getColorType(): ?string
    {
        return $this->colorType;
    }

    public function setColorType(string $colorType): self
    {
        $this->colorType = $colorType;

        return $this;
    }

    public function getStripeSk(): ?string
    {
        return $this->stripeSk;
    }

    public function setStripeSk(string $stripeSk): self
    {
        $this->stripeSk = $stripeSk;

        return $this;
    }

    public function getAgoraAppId(): ?string
    {
        return $this->agoraAppId;
    }

    public function setAgoraAppId(string $agoraAppId): self
    {
        $this->agoraAppId = $agoraAppId;

        return $this;
    }

    public function getAgoraAppCertificate(): ?string
    {
        return $this->agoraAppCertificate;
    }

    public function setAgoraAppCertificate(string $agoraAppCertificate): self
    {
        $this->agoraAppCertificate = $agoraAppCertificate;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(?string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getFreeTicket(): ?int
    {
        return $this->freeTicket;
    }

    public function setFreeTicket(?int $freeTicket): self
    {
        $this->freeTicket = $freeTicket;

        return $this;
    }

    public function getMinBeforeEntry(): ?int
    {
        return $this->minBeforeEntry;
    }

    public function setMinBeforeEntry(int $minBeforeEntry): self
    {
        $this->minBeforeEntry = $minBeforeEntry;

        return $this;
    }

    public function getCurrencySymbol(): ?string
    {
        return $this->currencySymbol;
    }

    public function setCurrencySymbol(?string $currencySymbol): self
    {
        $this->currencySymbol = $currencySymbol;

        return $this;
    }

    public function getMinAfterEntry(): ?int
    {
        return $this->minAfterEntry;
    }

    public function setMinAfterEntry(int $minAfterEntry): self
    {
        $this->minAfterEntry = $minAfterEntry;

        return $this;
    }

}
