<?php

namespace App\Repository;

use App\Entity\Schedule;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Schedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Schedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Schedule[]    findAll()
 * @method Schedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScheduleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Schedule::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Schedule $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Schedule $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @return Schedule[] Returns an array of Schedule objects
     */

    public function findByWeek($from, $to)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.dateTime >= :from')
            ->andWhere('s.dateTime <= :to')
            // ->andwhere('s.student is NULL')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->orderBy('s.dateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Schedule[] Returns an array of Schedule objects
     */

    public function findByWeekAndUserNotNull($from, $to)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.dateTime >= :from')
            ->andWhere('s.dateTime <= :to')
            ->andwhere('s.student is not NULL')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->orderBy('s.dateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByWeekAndUser($from, $to, $user)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.dateTime >= :from')
            ->andWhere('s.dateTime <= :to')
            ->andwhere('s.student = :user')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('user', $user->getId())
            ->orderBy('s.dateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }
    public function findHistory($today)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.dateTime < :today')
            ->andwhere('s.student is not NULL')
            ->setParameter('today', $today)
            ->orderBy('s.dateTime', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findHistoryByUser($today, $user)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.dateTime < :today')
            ->andwhere('s.student = :user')
            ->setParameter('today', $today)
            ->setParameter('user', $user->getId())
            ->orderBy('s.dateTime', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findOneByDateTime($dateTime): ?Schedule
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.dateTime = :val')
            ->setParameter('val', $dateTime)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
