<?php

namespace App\Service\Calculator;

use DateTimeZone;
use App\Repository\SettingRepository;

class TimezoneService
{
	protected $settingRepository;

	// List of seconds difference from standard time(GMT)
	// Associative array ["Europe/Paris" => 7200]
	public $timezoneTable;

	// List of timezone
	// Associative array ["Europe/Paris" => "Europe/Paris"]
	public $timezoneList;

	public function __construct(SettingRepository $settingRepository)
	{
		$this->settingRepository = $settingRepository;
		$this->timezoneTable = [];
		$this->timezoneList = [];
		$timezone = DateTimeZone::listIdentifiers();

		foreach ($timezone as $val) {

			date_default_timezone_set($val);
			date_default_timezone_get();
			$localTime = strtotime(date("Y-m-d H:i:s"));
			$gmtTime = strtotime(gmdate("Y-m-d H:i:s"));
			$diff = $localTime - $gmtTime;
			$this->timezoneList[$val] = $val;
			$this->timezoneTable[$val] = $diff;
		}
	}

	public function getGmtTimeDifference($timezone)
	{
		$timeDifference = $this->timezoneTable[$timezone];

		if ($timeDifference > -1) {
			$strTimeDifference = '+' . strval($timeDifference) . ' second';
		} else {
			$strTimeDifference = strval($timeDifference) . ' second';
		}
		return $strTimeDifference;
	}

	public function getGmtTime($time, $timezone)
	{
		if ($this->timezoneTable[$timezone] > -1) {
			$timeDiff = (strval($this->timezoneTable[$timezone]));
			$modifyDiff = "-$timeDiff second";
		} else {
			$timeDiff = (strval(($this->timezoneTable[$timezone]) * -1));
			$modifyDiff = "+$timeDiff second";
		}
		$gmtTime = $time->modify($modifyDiff);
		return $gmtTime;
	}
}
