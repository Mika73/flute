<?php

namespace App\Service\Calculator;

use DateTime;
use DateInterval;
use App\Repository\SettingRepository;

class DateCalculatorService
{
	protected $settingRepository;

	public function __construct(SettingRepository $settingRepository)
	{
		$this->settingRepository = $settingRepository;
	}

	public function getGmtPeriod($is_earlier)
	{
		$setting = $this->settingRepository->find(1);
		date_default_timezone_set('UTC');
		$gmtFrom = new DateTime();
		$gmtTo = clone $gmtFrom;
		$gmtTo = $gmtTo->add(new DateInterval('P7D'));

		// Search for reservations even if the class start time has already passed
		if ($is_earlier) {
			$minAfterEntry = '-' . $setting->getMinAfterEntry() . ' minutes';
			$interval = DateInterval::createFromDateString($minAfterEntry);
			$gmtFrom = $gmtFrom->add($interval);
		}

		return [$gmtFrom, $gmtTo];
	}

	public function getGmtTimeBeforeLesson()
	{
		$setting = $this->settingRepository->find(1);
		date_default_timezone_set('UTC');
		$gmtNow = new DateTime();

		$lessonTime = '-' . $setting->getLessonTime() . ' minutes';
		$interval = DateInterval::createFromDateString($lessonTime);
		$gmtNow = $gmtNow->add($interval);

		return $gmtNow;
	}
}
