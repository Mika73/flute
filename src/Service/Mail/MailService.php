<?php

namespace App\Service\Mail;

use Symfony\Component\Mime\Address;
use App\Repository\SettingRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class MailService
{
	protected $mailer;
	protected $settingRepository;

	public function __construct(MailerInterface $mailer, SettingRepository $settingRepository)
	{
		$this->mailer = $mailer;
		$this->settingRepository = $settingRepository;
	}

	public function sendContactEmail($lastname, $firstname, $email, $message)
	{
		$setting = $this->settingRepository->find(1);
		$email = (new TemplatedEmail())
			->from(new Address($setting->getAdminMail(), $setting->getSiteTitle()))
			->to($setting->getAdminMail())
			->subject('Contact mail from '.$setting->getSiteTitle())
			->htmlTemplate('mail/contact.html.twig')
			->context([
				'lastname' => $lastname,
				'firstname' => $firstname,
				'mail' => $email,
				'message' => $message
			]);
		$this->mailer->send($email);
	}

	public function sendOrderEmail($user, $order, $total, $hostName)
	{
		$setting = $this->settingRepository->find(1);
		$email = (new TemplatedEmail())
			->from(new Address($setting->getAdminMail(), $setting->getSiteTitle()))
			->to($user->getEmail())
			->subject('Thank you')
			->htmlTemplate('mail/confirm_order.html.twig')
			->context([
				'user'	=> $user,
				'total' => $total,
				'order' => $order,
				'host' => $hostName
			]);

		$this->mailer->send($email);
	}

}
