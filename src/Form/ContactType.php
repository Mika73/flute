<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
	public const HONEYPOT_FIELD_NAME = 'address';
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('firstname', TextType::class)
			->add('lastname', TextType::class)
			->add('email', EmailType::class)
			->add(self::HONEYPOT_FIELD_NAME, TextType::class, ['required' => false])
			->add('message', TextareaType::class, [
				'attr' => ['class' => 'specific-textarea']
			])
			->add('privacy', CheckboxType::class, [
				'label'    => 'I understand and agree to the privacy policy.'
			]);
	}
}
