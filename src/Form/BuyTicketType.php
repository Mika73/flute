<?php

namespace App\Form;

use App\Entity\Product;
use Doctrine\ORM\EntityRepository;
use App\Repository\SettingRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuyTicketType extends AbstractType
{
  public function __construct(SettingRepository $settingRepository)
  {
      $this->settingRepository = $settingRepository;
  }

  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('product', EntityType::class, [
        'class' => Product::class,
        'query_builder' => function (EntityRepository $er) {
          return $er->createQueryBuilder('p')
            ->orderBy('p.price', 'ASC');
        },
        'choice_label' => function ($product) {
          $setting = $this->settingRepository->find(1);
          $name = $product->getName();
          $price = $product->getPrice()->getPrice();
          $currencySymbol = $setting->getCurrencySymbol();
          if($currencySymbol)return $name . ' ' . sprintf("%.2f", $price). ' '.$currencySymbol;
          $currency = $setting->getCurrency();
          return $name . ' ' . sprintf("%.2f", $price). ' '.$currency;
        },
        'multiple' => false,
        'expanded' => true,
        'mapped' => false
      ]);
  }

  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'data_class' => Product::class,
    ]);
  }
}
