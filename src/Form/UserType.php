<?php

namespace App\Form;

use App\Entity\User;
use App\Repository\SettingRepository;
use Symfony\Component\Form\AbstractType;
use App\Service\Calculator\TimezoneService;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    protected $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $timezoneService = new TimezoneService($this->settingRepository);
        $setting = $this->settingRepository->find(1);

        $builder
            ->add('lastname', TextType::class, [
                'attr' => ['class' => 'form-control mb-2'],
                'label' => 'Last Name',
            ])
            ->add('firstname', TextType::class, [
                'attr' => ['class' => 'form-control mb-2'],
                'label' => 'First Name',
            ])
            ->add('email', EmailType::class, [
                'attr' => ['class' => 'form-control mb-2'],
                'label' => 'Email',
            ])
            ->add('locale', ChoiceType::class, [
                'attr' => ['class' => 'form-control mb-2'],
                'label' => 'Language',
                'expanded' => false,
                'multiple' => false,
                'required'   => false,
                'choices'  => [
                    'English' => 'en',
                    'Français' => 'fr',
                ]
            ])
            ->add('timezone', ChoiceType::class, [
                'choices'  => $timezoneService->timezoneList,
                'expanded' => false,
                'multiple' => false,
                'required'   => true,
                'label' => 'Time zone',
            ]);
        if ($options['controller'] === 'user') {
            $builder
                ->add('roles', ChoiceType::class, [
                    'attr' => ['class' => 'form-control mb-2'],
                    'multiple' => true,
                    'choices'  => [
                        'Student' => 'ROLE_USER',
                        'Admin' => 'ROLE_ADMIN',
                    ]
                ])
                ->add('ticket', IntegerType::class, [
                    'attr' => ['class' => 'form-control mb-2'],
                    'required'   => false
                ]);
        }
        if ($options['controller'] === 'registration') {
            $builder
                ->add('timezone', ChoiceType::class, [
                    'attr' => ['class' => 'form-control mb-2'],
                    'choices'  => $timezoneService->timezoneList,
                    'expanded' => false,
                    'multiple' => false,
                    'required'   => true,
                    'label' => 'Time zone',
                    'data' => $setting->getTimezone()
                ])
                ->add('agreeTerms', CheckboxType::class, [
                    'mapped' => false,
                    'constraints' => [
                        new IsTrue([
                            'message' => 'You should agree to our terms.',
                        ]),
                    ],
                    'attr' => ['class' => 'form-check-input input_check'],
                    'label' => 'By checking out you agree with our Terms of Service. We will process your personal data for the fulfillment of your order and other purposes as per our Privacy Policy'
                ])
                ->add('plainPassword', PasswordType::class, [
                    // instead of being set onto the object directly,
                    // this is read and encoded in the controller
                    'mapped' => false,
                    'attr' => ['autocomplete' => 'new-password'],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a password',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Your password should be at least {{ limit }} characters',
                            // max length allowed by Symfony for security reasons
                            'max' => 4096,
                        ]),
                    ],
                    'attr' => ['class' => 'form-control mb-2'],
                    'label' => 'Password'
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'controller' => '',
        ]);
        $resolver->setAllowedTypes('controller', 'string');
    }
}
