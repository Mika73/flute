<?php

namespace App\Form;

use App\Entity\Setting;
use App\Repository\SettingRepository;
use Symfony\Component\Form\AbstractType;
use App\Service\Calculator\TimezoneService;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SettingType extends AbstractType
{
	protected $settingRepository;

	public function __construct(SettingRepository $settingRepository)
	{
		$this->settingRepository = $settingRepository;
	}
	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
		$timezoneService = new TimezoneService($this->settingRepository);
		$builder
			->add('siteTitle')
			->add('metaDescription')
			->add('logoFile', VichImageType::class, [
				'required' => false,
				'allow_delete' => true,
				'delete_label' => 'Delete',
				'download_label' => 'Download',
				'download_uri' => true,
				'image_uri' => true,
				// 'imagine_pattern' => '...',
				'asset_helper' => true,
			])
			->add('adminMail', EmailType::class)
			->add('currency')
			->add('currencySymbol')
			->add('freeTicket')
			->add('lessonTime', ChoiceType::class, [
				'choices'  => [
					'25 min.' => 25,
					'50 min.' => 50,
				],
				'expanded' => true,
				'multiple' => false,
				'required'   => true
			])
			->add('minBeforeEntry')
			->add('minAfterEntry')
			->add('colorType', ChoiceType::class, [
				'choices'  => [
					'Cold' => 'Cold',
					'Warm' => 'Cold',
					'Dark' => 'Dark',
					'Pastel' => 'Pastel'
				],
				'expanded' => false,
				'multiple' => false,
				'required'   => true
			])
			->add('stripeSk')
			->add('agoraAppId')
			->add('agoraAppCertificate')
			->add('timezone', ChoiceType::class, [
				'choices'  => $timezoneService->timezoneList,
				'expanded' => false,
				'multiple' => false,
				'required'   => true
			]);
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => Setting::class,
		]);
	}
}
