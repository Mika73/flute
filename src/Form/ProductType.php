<?php

namespace App\Form;

use App\Entity\Price;
use App\Entity\Product;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('ticketNumber')
            ->add('price', EntityType::class, [
				'class' => Price::class,
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('p')
						->orderBy('p.price', 'ASC');
				},
				'choice_label' => function ($price) {
                    return $price->getPrice();
                    ;
				}])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
