<?php

namespace App\Controller;

use DateTime;
use DateTimeZone;
use App\Entity\Schedule;
use App\Repository\UserRepository;
use App\Repository\OrderRepository;
use App\Repository\SettingRepository;
use App\Repository\ScheduleRepository;
use App\Service\RtcToken\RtcTokenBuilder;
use Doctrine\Persistence\ManagerRegistry;
use App\Service\Calculator\TimezoneService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/ticket/count", name="api_ticket_count",methods={"GET"})
     */
    public function apiCount(): JsonResponse
    {
        if ($this->getUser()) {
            return new JsonResponse([
                'ticketNumber' => $this->getUser()->getTicket()
            ]);
        }
        return new JsonResponse([
            'ticketNumber' => null
        ]);
    }

    /**
     * @Route("/api/rtctoken/get", name="api_rtctoken_get", methods={"POST"})
     */
    public function apiTokenGet($agoraAppId, $agoraAppCertificate, Request $request, RtcTokenBuilder $rtcTokenBuilder): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $appID = $agoraAppId;
        $appCertificate = $agoraAppCertificate;
        $channelName = 'lesson' . $data['lesson'];
        $uid = intval($data['data']);
        $role = RtcTokenBuilder::RoleAttendee;
        $expireTimeInSeconds = 3600;
        $currentTimestamp = (new DateTime("now", new DateTimeZone('UTC')))->getTimestamp();
        $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;
        $token = $rtcTokenBuilder::buildTokenWithUid($appID, $appCertificate, $channelName, $uid, $role, $privilegeExpiredTs);

        return new JsonResponse([
            'appId' => $appID,
            'channel' => $channelName,
            'token' => $token,
            'uid' => $uid,
            'aid' => 11
        ]);
    }

    /**
     *
     * @Route("/api/reservation/update", name="api_reservation_update", methods={"PUT"})
     */
    public function reservationUpdate(Request $request, ManagerRegistry $doctrine, ScheduleRepository $scheduleRepository): JsonResponse
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $data = json_decode($request->getContent(), true);
        $user = $this->getUser();
        $entityManager = $doctrine->getManager();

        $gmtSearchDate = \DateTime::createFromFormat('Y-m-d H:i:s', $data['date-time']);
        $schedule = $scheduleRepository->findOneByDateTime($gmtSearchDate);

        if ($schedule->getStudent() == null) {
            if ($user->getTicket() > 0) {
                $schedule->setStudent($user);
                $user->setTicket($user->getTicket() - 1);
                $status = 'Reserved';
            } else {
                return new JsonResponse([
                    'dateTime' => $data['date-time'],
                    'status' => 'noTicket',
                    'ticketNo' => $user->getTicket()
                ]);
            }
        } else {
            $user->setTicket($user->getTicket() + 1);
            $schedule->setStudent(null);
            $status = 'Cancel Reservation';
        }
        $entityManager->persist($schedule);
        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse([
            'dateTime' => $data['date-time'],
            'status' => $status,
            'ticketNo' => $user->getTicket()
        ]);
    }

    /**
     * @Route("/api/stripe/webhook", name="stripe_webhook")
     */
    public function webhook($stripeSK, UserRepository $userRepository, OrderRepository $orderRepository): JsonResponse
    {
        \Stripe\Stripe::setApiKey($stripeSK);
        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;
        $endpoint_secret = 'whsec_JSSWpKhFgZ1zOUQ6PRcEjsNaISPTAEXt';

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload,
                $sig_header,
                $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit('{Invalid payload}');
        } catch (\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit('{Invalid signature}');
        }

        // Handle the event
        if ($event->type === 'checkout.session.completed') {
            $payment = $event->data->object;
            $order = $orderRepository->findOneByStripSessionId($payment->id);
            $user = $order->getUser();
            $gainTicketNumber = $order->getProduct()->getTicketNumber();
            $user->setTicket($user->getTicket() + $gainTicketNumber);
            $order->setStripePayment($payment);
            $userRepository->add($user);
            $orderRepository->add($order);
        }
        http_response_code(200);
        return new JsonResponse('ok');
    }
    /**
     * @Route("/admin/api/schedule/update", name="api_schedule_add", methods={"POST"})
     */
    public function scheduleUpdate(Request $request, ManagerRegistry $doctrine, ScheduleRepository $scheduleRepository, TimezoneService $timezoneService, SettingRepository $settingRepository): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $entityManager = $doctrine->getManager();

        $setting = $settingRepository->find(1);
        $timezone = $setting->getTimezone();
        $searchDate = \DateTime::createFromFormat('Y-m-d H:i:s', $data['date-time']);
        $gmtSearchDate = $timezoneService->getGmtTime($searchDate, $timezone);
        $schedule = $scheduleRepository->findOneByDateTime($gmtSearchDate);
        
        if ($schedule) {
            // This slot has already been reserved by a student and its status cannot be changed
            if ($schedule->getStudent()) {
                $status = 'Available';
            } else {
                $entityManager->remove($schedule);
                $entityManager->flush();
                $status = 'Off';
            }
        } else {
            $schedule = new Schedule();
            $newDate = \DateTime::createFromFormat('Y-m-d H:i:s', $data['date-time']);
            $gmtNewDate = $timezoneService->getGmtTime($newDate, $timezone);
            $schedule->setDateTime($gmtNewDate);
            $entityManager->persist($schedule);
            $entityManager->flush();
            $status = 'Available';
        }

        return new JsonResponse([
            'status' => $status,
            'dateTime' => $data['date-time'],
        ]);
    }
}
