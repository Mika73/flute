<?php

namespace App\Controller;

use DateTime;
use App\Entity\Order;
use Stripe\StripeClient;
use App\Form\BuyTicketType;
use App\Repository\ProductRepository;
use App\Repository\SettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TicketController extends AbstractController
{
    /**
     * @Route("/ticket", name="app_ticket")
     */
    public function index($stripeSK, SettingRepository $settingRepository, Request $request, ProductRepository $productRepository, EntityManagerInterface $manager): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $setting = $settingRepository->find(1);
        $bookNow = false;

        if ($request->get('session_id') && $request->get('transaction') === 'success') {
            $this->addFlash('success', 'Payment successfully completed.');
            $bookNow = true;
        } elseif ($request->get('transaction') === 'cancel') {
            $this->addFlash('error', 'Failed to pay. Please try again.');
        }

        $form = $this->createForm(BuyTicketType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->get('product')->getData();
            $stripe = new StripeClient($setting->getStripeSk());

            $session = $stripe->checkout->sessions->create([
                'success_url' => $this->generateUrl('app_ticket', [], UrlGeneratorInterface::ABSOLUTE_URL) . '?transaction=success&session_id={CHECKOUT_SESSION_ID}',
                'cancel_url' => $this->generateUrl('app_ticket', [], UrlGeneratorInterface::ABSOLUTE_URL) . '?transaction=cancel',
                'customer_email' => $this->getUser()->getEmail(),
                'client_reference_id' => $this->getUser()->getId(),
                'line_items' => [
                    [
                        'price_data' => [
                            'currency' => $setting->getCurrency(),
                            'unit_amount' => $product->getPrice()->getPrice() * 100,
                            'product_data' => [
                                'name' => 'Tickets for ' . $product->getName(),
                                'metadata' => [
                                    'userId' => $this->getUser()->getId(),
                                    'gainTicketNumber' => $product->getTicketNumber(),
                                ]
                            ]
                        ],
                        'quantity' => 1,
                    ],
                ],
                'mode' => 'payment',
            ]);

            // Stores order details in database
            $order = new Order();
            $order->setUser($this->getUser());
            $order->setProduct($product);

            // Order DateTime is stored in GMT DateTime
            date_default_timezone_set('UTC');
            $gmtNow = new DateTime();
            $order->setDateTime($gmtNow);

            // Retrieve the order by stripe session ID when the webhook is later received
            $order->setStripSessionId($session->id);
            $manager->persist($order);
            $manager->flush();
            return $this->redirect($session->url, Response::HTTP_SEE_OTHER);
        }

        return $this->render('ticket/index.html.twig', [
            'ticketNumber' => $this->getUser()->getTicket(),
            'products' => $productRepository->findAll(),
            'form' => $form->createView(),
            'setting' => $setting,
            'bookNow' => $bookNow
        ]);
    }
}
