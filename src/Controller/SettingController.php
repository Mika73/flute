<?php

namespace App\Controller;

use App\Entity\Setting;
use App\Form\SettingType;
use App\Repository\SettingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/setting")
 */
class SettingController extends AbstractController
{
    /**
     * @Route("/new", name="app_setting_new", methods={"GET", "POST"})
     */
    public function new(Request $request, SettingRepository $settingRepository): Response
    {
        $newSetting = new Setting();
        $form = $this->createForm(SettingType::class, $newSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $settingRepository->add($newSetting);
            return $this->redirectToRoute('app_setting_index', [
                'setting' => $settingRepository->find(1)
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('setting/new.html.twig', [
            'new_setting' => $newSetting,
            'form' => $form,
            'setting' => $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/edit", name="app_setting_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, SettingRepository $settingRepository): Response
    {
        $setting = $settingRepository->find(1);
        $form = $this->createForm(SettingType::class, $setting, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dump($setting);
            $settingRepository->add($setting);
            $this->addFlash('success', 'Settings update succeeded');
        }

        return $this->renderForm('setting/edit.html.twig', [
            'edit_setting' => $setting,
            'form' => $form,
            'setting' => $settingRepository->find(1)
        ]);
    }
}
