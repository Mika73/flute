<?php

namespace App\Controller;

use App\Entity\Order;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use App\Repository\SettingRepository;
use App\Service\Calculator\TimezoneService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Migrations\Tools\Console\Command\UpToDateCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OrderController extends AbstractController
{
    /**
     * @Route("/admin/order", name="app_order_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, SettingRepository $settingRepository, OrderRepository $orderRepository, TimezoneService $timezoneService): Response
    {
        // Calculate the difference between user time and GMT time
        $timeDifference = $timezoneService->getGmtTimeDifference($this->getUser()->getTimezone());
        $orders = $paginator->paginate(
            $orderRepository->findAll(),
            $request->query->getInt('page', 1), /*page number*/
            20 /*limit per page*/
        );
        return $this->render('order/index.html.twig', [
            'orders' => $orders,
            'setting' => $settingRepository->find(1),
            'timeDifference' => $timeDifference
        ]);
    }

    /**
     * @Route("/admin/order/{id}", name="app_order_show", methods={"GET"})
     */
    public function show(SettingRepository $settingRepository, Order $order, TimezoneService $timezoneService): Response
    {
        $stripePayment = $order->getStripePayment();

        // Calculate the difference between user time and GMT time
        $timeDifference = $timezoneService->getGmtTimeDifference($this->getUser()->getTimezone());

        return $this->render('order/show.html.twig', [
            'order' => $order,
            'paymentObject' => $stripePayment,
            'setting' => $settingRepository->find(1),
            'timeDifference' => $timeDifference
        ]);
    }

    /**
     * @Route("/order/user/", name="app_order_user_index", methods={"GET"})
     */
    public function indexUser(SettingRepository $settingRepository, OrderRepository $orderRepository, TimezoneService $timezoneService): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $orders = [];
        $results = $orderRepository->findByUser($this->getUser());

        // Calculate the difference between user time and GMT time
        $timeDifference = $timezoneService->getGmtTimeDifference($this->getUser()->getTimezone());

        // Only order records that have been successfully settled will be output to the history
        foreach ($results as $result) {
            if ($result->getStripePayment()) {
                $orders[] = $result;
            }
        }

        return $this->render('order/user_index.html.twig', [
            'orders' => $orders,
            'setting' => $settingRepository->find(1),
            'timeDifference' => $timeDifference
        ]);
    }
}
