<?php

namespace App\Controller;

use DateTime;
use DateInterval;
use App\Repository\SettingRepository;
use App\Repository\ScheduleRepository;
use App\Service\Calculator\TimezoneService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/schedule")
 */
class ScheduleController extends AbstractController
{
    private $dailySchedule;
    private $timezone;
    private $settingRepository;
    private $scheduleRepository;
    private $timezoneService;

    public function __construct(
        SettingRepository $settingRepository,
        ScheduleRepository $scheduleRepository,
        TimezoneService $timezoneService
    ) {
        $this->settingRepository = $settingRepository;
        $this->scheduleRepository = $scheduleRepository;
        $this->timezoneService = $timezoneService;
    }
    /**
     * @Route("/", name="app_schedule_index", methods={"GET"})
     */
    public function index(): Response
    {
        $setting = $this->settingRepository->find(1);
        $this->timezone = $setting->getTimezone();
        date_default_timezone_set($this->timezone);

        $from = new DateTime();
        $to = clone $from;
        $to = $to->add(new DateInterval('P7D'));
        $scheduleTable = [];

        for ($i = 0; $i < 7; $i++) {

            $date = clone $from;
            $date = clone $date->modify("+$i day");
            $this->dailySchedule = [];

            for ($j = 0; $j < 24; $j++) {
                if ($j < 10) {
                    $startTime = "0$j:00:00";
                    $this->editDailyScheduleTable($date, $startTime);
                    $startTime = "0$j:30:00";
                    $this->editDailyScheduleTable($date, $startTime);
                } else {
                    $startTime = "$j:00:00";
                    $this->editDailyScheduleTable($date, $startTime);
                    $startTime = "$j:30:00";
                    $this->editDailyScheduleTable($date, $startTime);
                }
            }
            $scheduleTable[] = [
                'date' => $date,
                'schedule' => $this->dailySchedule
            ];
        }
        return $this->render('schedule/index.html.twig', [
            'scheduleTable' => $scheduleTable,
            'setting' => $setting
        ]);
    }

    /**
     * Create a 24-hour schedule for a day in an associative array
     *
     * @param [DateTime] $date
     * @param [String] $startTime
     *
     * @return void
     */
    private function editDailyScheduleTable($date, $startTime)
    {
        $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y-m-d') . ' ' . $startTime);
        $gmtStartTime = $this->timezoneService->getGmtTime($dateTime, $this->timezone);
        $status = $this->getSlotStatus($gmtStartTime);
        $this->dailySchedule[] = [
            'start-time' => $startTime,
            'on-off' => $status
        ];
    }

    /**
     * Return slot status
     *
     * @param [GMT DateTime] $gmtStartTime
     *
     * @return string('Closed' or 'Available' or 'Off')
     */
    private function getSlotStatus($gmtStartTime): string
    {
        date_default_timezone_set('UTC');
        $gmtNow = new DateTime();
        if ($gmtStartTime < $gmtNow) {
            return 'Closed';
        }
        $schedule = $this->scheduleRepository->findOneByDateTime($gmtStartTime);
        if ($schedule) {
            if($schedule->getStudent()){
                return 'Reserved';
            }
            return 'Available';
        }
        return 'Off';
    }
}
