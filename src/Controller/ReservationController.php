<?php

namespace App\Controller;

use App\Repository\SettingRepository;
use App\Repository\ScheduleRepository;
use App\Service\Calculator\TimezoneService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Calculator\DateCalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReservationController extends AbstractController
{
    /**
     * Displays a list of user reservations
     * @Route("/reservation", name="app_reservation")
     */
    public function index(
        TimezoneService $timezoneService,
        DateCalculatorService $dateCalculatorService,
        SettingRepository $settingRepository,
        ScheduleRepository $scheduleRepository
    ): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // Calculate the difference between user time and GMT time
        $timeDifference = $timezoneService->getGmtTimeDifference($this->getUser()->getTimezone());

        // Get GMT dates for 1 week from today
        list($from, $to) = $dateCalculatorService->getGmtPeriod(true);

        // Search the schedule repository for a user ID and the one-week time period just retrieved
        $myReservation = $scheduleRepository->findByWeekAndUser($from, $to, $this->getUser());

        return $this->render('home/reservation_list_user.html.twig', [
            'now' => $from,
            'myReservation' => $myReservation,
            'setting' => $settingRepository->find(1),
            'timeDifference' => $timeDifference
        ]);
    }
    /**
     * @Route("/reservation/new", name="app_reservation_new")
     */
    public function new(DateCalculatorService $dateCalculatorService, TimezoneService $timezoneService, SettingRepository $settingRepository, ScheduleRepository $scheduleRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        list($from, $to) = $dateCalculatorService->getGmtPeriod(false);
        $availables = $scheduleRepository->findByWeek($from, $to);
        $availableTable = [];

        for ($i = 0; $i < 7; $i++) {
            $availableSlots = [];
            $date = clone $from;
            foreach ($availables as $available) {
                if ($available->getDateTime()->format('Y-m-d') == $date->format('Y-m-d')) {
                    if ($available->getStudent() == null) {
                        $availableSlots[] = [
                            'available' => $available->getDateTime(),
                            'myReservation' => null
                        ];
                    } else if ($available->getStudent() == $this->getUser()) {
                        $availableSlots[] = [
                            'available' => null,
                            'myReservation' => $available->getDateTime()
                        ];
                    }
                }
            }
            $availableTable[] = [
                'date' => $date,
                'schedule' => $availableSlots
            ];
            $from->modify('+1 day');
        }

        $timeDifference = $timezoneService->getGmtTimeDifference($this->getUser()->getTimezone());
        return $this->render('home/new_reservation.html.twig', [
            'availableTable' => $availableTable,
            'setting' => $settingRepository->find(1),
            'timeDifference' => $timeDifference
        ]);
    }

    /**
     * Displays a list of ALL user reservations for admin
     * @Route("/admin/reservation", name="app_admin_reservation")
     */
    public function admin(DateCalculatorService $dateCalculatorService, SettingRepository $settingRepository, ScheduleRepository $scheduleRepository, TimezoneService $timezoneService): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $setting = $settingRepository->find(1);
        // Calculate the difference between setting timezone time and GMT time
        $timeDifference = $timezoneService->getGmtTimeDifference($setting->getTimezone());

        // Get GMT dates for 1 week from today
        list($from, $to) = $dateCalculatorService->getGmtPeriod(true);

        // Search the schedule repository for all user and the one-week time period just retrieved
        $myReservation = $scheduleRepository->findByWeekAndUserNotNull($from, $to);

        return $this->render('home/reservation_list_admin.html.twig', [
            'now' => $from,
            'myReservation' => $myReservation,
            'setting' => $setting,
            'timeDifference' => $timeDifference
        ]);
    }
}
