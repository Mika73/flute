<?php

namespace App\Controller;

use App\Entity\Price;
use App\Form\PriceType;
use App\Repository\PriceRepository;
use App\Repository\SettingRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/price")
 */
class PriceController extends AbstractController
{
    /**
     * @Route("/", name="app_price_index", methods={"GET"})
     */
    public function index(SettingRepository $settingRepository, PriceRepository $priceRepository): Response
    {
        return $this->render('price/index.html.twig', [
            'prices' => $priceRepository->findAll(),
            'setting'=> $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/new", name="app_price_new", methods={"GET", "POST"})
     */
    public function new(SettingRepository $settingRepository, Request $request, PriceRepository $priceRepository): Response
    {
        $price = new Price();
        $form = $this->createForm(PriceType::class, $price);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $priceRepository->add($price);
            return $this->redirectToRoute('app_price_index', [
                'setting'=> $settingRepository->find(1)
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('price/new.html.twig', [
            'price' => $price,
            'form' => $form,
            'setting'=> $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/{id}", name="app_price_show", methods={"GET"})
     */
    public function show(SettingRepository $settingRepository, Price $price): Response
    {
        return $this->render('price/show.html.twig', [
            'price' => $price,
            'setting'=> $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_price_edit", methods={"GET", "POST"})
     */
    public function edit(SettingRepository $settingRepository, Request $request, Price $price, PriceRepository $priceRepository): Response
    {
        $form = $this->createForm(PriceType::class, $price);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $priceRepository->add($price);
            return $this->redirectToRoute('app_price_index', [
                'setting'=> $settingRepository->find(1)
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('price/edit.html.twig', [
            'price' => $price,
            'form' => $form,
            'setting'=> $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/{id}", name="app_price_delete", methods={"POST"})
     */
    public function delete(SettingRepository $settingRepository, Request $request, Price $price, PriceRepository $priceRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$price->getId(), $request->request->get('_token'))) {
            $priceRepository->remove($price);
        }

        return $this->redirectToRoute('app_price_index', [
            'setting'=> $settingRepository->find(1)
        ], Response::HTTP_SEE_OTHER);
    }
}
