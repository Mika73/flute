<?php

namespace App\Controller;

use DateTime;
use App\Repository\SettingRepository;
use App\Repository\ScheduleRepository;
use App\Service\Calculator\TimezoneService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Calculator\DateCalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LessonHistoryController extends AbstractController
{
    /**
     * @Route("/lesson/history", name="app_lesson_history")
     */
    public function index(TimezoneService $timezoneService, DateCalculatorService $dateCalculatorService, SettingRepository $settingRepository, ScheduleRepository $scheduleRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $setting = $settingRepository->find(1);

        // Anything that has not yet completed its lesson is not included in the history
        $timeAfterLesson = $dateCalculatorService->getGmtTimeBeforeLesson();

        if ($this->isGranted('ROLE_ADMIN')) {
            $lessons = $scheduleRepository->findHistory($timeAfterLesson);
            $totalLessonCount = count($lessons);
            $totalLessonTime = count($lessons) * $setting->getLessonTime();
            $setting = $settingRepository->find(1);
            $timeDifference = $timezoneService->getGmtTimeDifference($setting->getTimezone());
            $lessons = $paginator->paginate(
                $scheduleRepository->findHistory($timeAfterLesson),
                $request->query->getInt('page', 1), /*page number*/
                20 /*limit per page*/
            );
            return $this->render('lesson_history/index_admin.html.twig', [
                'setting' => $settingRepository->find(1),
                'lessons' => $lessons,
                'total_lesson_count' => $totalLessonCount,
                'total_lesson_time' => $totalLessonTime,
                'timeDifference' => $timeDifference
            ]);
        }
        $lessons = $scheduleRepository->findHistoryByUser($timeAfterLesson, $this->getUser());
        $totalLessonCount = count($lessons);
        $totalLessonTime = count($lessons) * $setting->getLessonTime();

        $lessons = $paginator->paginate(
            $scheduleRepository->findHistoryByUser($timeAfterLesson, $this->getUser()),
            $request->query->getInt('page', 1), /*page number*/
            15 /*limit per page*/
        );

        $timeDifference = $timezoneService->getGmtTimeDifference($this->getUser()->getTimezone());

        return $this->render('lesson_history/index.html.twig', [
            'setting' => $settingRepository->find(1),
            'lessons' => $lessons,
            'total_lesson_count' => $totalLessonCount,
            'total_lesson_time' => $totalLessonTime,
            'timeDifference' => $timeDifference
        ]);
    }
}
