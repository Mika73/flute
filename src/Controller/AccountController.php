<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\UserPasswordType;
use App\Repository\UserRepository;
use App\Repository\SettingRepository;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AccountController extends AbstractController
{
    /**
     * @Route("/account/", name="app_account", methods={"GET","POST"})
     */
    public function index(Request $request, UserRepository $userRepository, SettingRepository $settingRepository, TranslatorInterface $translator): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user, ['controller' => 'account']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->add($user);
            $this->addFlash('success', $translator->trans('Settings update succeeded'));
        }

        return $this->renderForm('account/edit.html.twig', [
            'user' => $user,
            'form' => $form,
            'setting' => $settingRepository->find(1)
        ]);
    }
    /**
     * @Route("/{id}/account/password", name="password_edit", methods={"GET","POST"})
     */
    public function password(Request $request, User $user, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $manager, TranslatorInterface $translator): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(UserPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $form->get('old_password')->getData();
            if ($passwordHasher->isPasswordValid($user, $oldPassword)) {
                $repo = $manager->getRepository(User::class);
                $user = $repo->find($user->getId());
                $user->setPassword($passwordHasher->hashPassword(
                    $user,
                    $form->get('new_password')->getData()
                ));
                $manager->flush();
                $message = $translator->trans('Votre mot de passe a été modifié.');
                $this->addFlash('success', $message);
            } else {
                $form->addError(new FormError('Le mot de passe actuel renseigné est invalide.'));
            }
        }

        return $this->renderForm('account/password_edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }
}
