<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Service\Mail\MailService;
use App\Repository\SettingRepository;
use App\Repository\ScheduleRepository;
use App\Service\Calculator\TimezoneService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Calculator\DateCalculatorService;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(Request $request, TimezoneService $timezoneService, DateCalculatorService $dateCalculatorService, SettingRepository $settingRepository, ScheduleRepository $scheduleRepository): Response
    {
        if ($this->getUser()) {
            
            // Get GMT dates for 1 week from today
            list($from, $to) = $dateCalculatorService->getGmtPeriod(true);

            if ($this->isGranted('ROLE_ADMIN')) {
                $setting = $settingRepository->find(1);
                $allReservations = $scheduleRepository->findByWeekAndUserNotNull($from, $to);

                if (count($allReservations) === 0) {
                    $myFirstReservation = null;
                } else {
                    $myFirstReservation[] = $allReservations[0];
                }

                // Calculate the difference between setting timezone time and GMT time
                $timeDifference = $timezoneService->getGmtTimeDifference($setting->getTimezone());


                return $this->render('home/index_admin.html.twig', [
                    'now' => $from,
                    'myReservation' => $myFirstReservation,
                    'setting' => $setting,
                    'timeDifference' => $timeDifference
                ]);
            }

            $allReservations = $scheduleRepository->findByWeekAndUser($from, $to, $this->getUser());

            if (count($allReservations) === 0) {
                $myFirstReservation = null;
            } else {
                $myFirstReservation[] = $allReservations[0];
            }
            // Calculate the difference between user time and GMT time
            $timeDifference = $timezoneService->getGmtTimeDifference($this->getUser()->getTimezone());

            return $this->render('home/index.html.twig', [
                'now' => $from,
                'myReservation' => $myFirstReservation,
                'setting' => $settingRepository->find(1),
                'timeDifference' => $timeDifference
            ]);
        }
        return $this->render('home/index.html.twig', [
            'setting' => $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/contact", name="app_contact")
     */
    public function contact(SettingRepository $settingRepository, Request $request, MailService $mailer): Response
    {
        if ($this->isGranted('ROLE_USER')) {
            $user = $this->getUser();
            $data = [
                'firstname' => $user->getFirstname(),
                'lastname'  => $user->getLastname(),
                'email' => $user->getEmail()
            ];
            $form = $this->createForm(ContactType::class, $data);
        } else {
            $form = $this->createForm(ContactType::class);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Block bot emails
            if ($form->get('address')->getData()) {
                return $this->renderForm('home/contact.html.twig', [
                    'form' => $form,
                ]);
            }
            $lastname = $form->get('lastname')->getData();
            $firstname = $form->get('firstname')->getData();
            $email = $form->get('email')->getData();
            $message = $form->get('message')->getData();
            $mailer->sendContactEmail($lastname, $firstname, $email, $message);

            $this->addFlash('success', 'Your message was successfully sent.');

            return $this->redirectToRoute('app_contact', [
                'setting' => $settingRepository->find(1)
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('home/contact.html.twig', [
            'form' => $form,
            'setting' => $settingRepository->find(1)
        ]);
    }
}
