<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Repository\SettingRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="app_product_index", methods={"GET"})
     */
    public function index(SettingRepository $settingRepository,ProductRepository $productRepository): Response
    {
        return $this->render('product/index.html.twig', [
            'products' => $productRepository->findAll(),
            'setting'=> $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/new", name="app_product_new", methods={"GET", "POST"})
     */
    public function new(SettingRepository $settingRepository, Request $request, ProductRepository $productRepository): Response
    {
        $setting = $settingRepository->find(1);
        $data = [
            'currency'=> $setting->getCurrency(),
            'currencySymbol'=> $setting->getCurrencySymbol()
        ];
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product,[
            'data'=> $data,
            'data_class'=> null
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productRepository->add($product);
            return $this->redirectToRoute('app_product_index', [
                'setting'=> $settingRepository->find(1)
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product/new.html.twig', [
            'product' => $product,
            'form' => $form,
            'setting'=> $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/{id}", name="app_product_show", methods={"GET"})
     */
    public function show(SettingRepository $settingRepository, Product $product): Response
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
            'setting'=> $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_product_edit", methods={"GET", "POST"})
     */
    public function edit(SettingRepository $settingRepository, Request $request, Product $product, ProductRepository $productRepository): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productRepository->add($product);
            return $this->redirectToRoute('app_product_index', [
                'setting'=> $settingRepository->find(1)
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product/edit.html.twig', [
            'product' => $product,
            'form' => $form,
            'setting'=> $settingRepository->find(1)
        ]);
    }

    /**
     * @Route("/{id}", name="app_product_delete", methods={"POST"})
     */
    public function delete(SettingRepository $settingRepository, Request $request, Product $product, ProductRepository $productRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $productRepository->remove($product);
        }

        return $this->redirectToRoute('app_product_index', [
            'setting'=> $settingRepository->find(1)
        ], Response::HTTP_SEE_OTHER);
    }
}
